# Seminarios Modelización y Análisis de Recursos Naturales ([MAREN](http://www.maren.cure.edu.uy/))

![logo_maren](http://www.maren.cure.edu.uy/wp-content/uploads/2014/10/logo_maren_web.jpg)

## "abUse y disfrute de R"

### Consigna

> Rondas para presentar alguna herramienta de `R` que estemos usando (paquete, función, código, etc) y pensemos que puede ser útil para compartir con el grupo. 

### Presentación 

- Presentación online [Introducción a ggplot](https://guzmanlopez.gitlab.io/-/abuse-y-disfrute-de-r/-/jobs/280312211/artifacts/public/index.html#1)
- Descargar presentación en pdf [Introducción a ggplot](https://gitlab.com/guzmanlopez/abuse-y-disfrute-de-r/-/jobs/280312211/artifacts/raw/public/slides.pdf?inline=false)


### Ejecución en `R`

- Opción 1: descargar repositorio como [zip](https://gitlab.com/guzmanlopez/abuse-y-disfrute-de-r/-/archive/master/abuse-y-disfrute-de-r-master.zip) o [tar.gz](https://gitlab.com/guzmanlopez/abuse-y-disfrute-de-r/-/archive/master/abuse-y-disfrute-de-r-master.tar.gz).

- Opción 2 (sugerida): clonar repositorio utilizando `git`

```bash
# Con SSH
git clone git@gitlab.com:guzmanlopez/abuse-y-disfrute-de-r.git

# Con HTTPS
git clone https://gitlab.com/guzmanlopez/abuse-y-disfrute-de-r.git
```

- Para poder generar la presentación desde el archivo RMarkdown (`.Rmd`) es necsario instalar previamente las siguientes librerías de `R`:


```r
install.packages(xaringan)
devtools::install_github("hadley/emo")
```


